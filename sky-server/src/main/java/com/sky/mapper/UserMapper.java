package com.sky.mapper;

import com.sky.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.Map;

@Mapper
public interface UserMapper {
    /**
     * 根据openid 查询用户
     * @param openid
     * @return
     */
    @Select("select * from user where openid = #{openid}")
    User getById(String openid);

    /**
     * 根据 id 查询用户
     * @param userId
     * @return
     */
    @Select("select * from user where id = #{id}")
    User getByIda(Long userId);

    /**
     * 插入数据
     * @param user
     */
    void insert(User user);
    /**
     * 根据动态条件统计用户数量
     * @param map
     * @return
     */
    Integer countByMap(Map map);
}
